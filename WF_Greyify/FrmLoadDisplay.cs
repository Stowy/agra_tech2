﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using WF_Greyify;

namespace WF_LoadDisplayImage
{
    public partial class FrmLoadDisplay : Form
    {
        private const string TAG_REDIFY = "Redify";
        private const string TAG_GREYIFY_GET_SET = "GreyifyGetSet";
        private const string TAG_GREYIFY_LOCK_BITS = "GreyifyLockBits";
        private const string TAG_GREYIFY_UNSAFE = "GreyifyLockBitsUnsafe";
        private const string TAG_GREYIFY_PARALLEL = "GreyifyLockBitsUnsafeParallel";
        private const string TAG_MEDIAN = "Median";
        private readonly OpenFileDialog imagePicker;
        private readonly SaveFileDialog imageSaver;
        private Bitmap bitmap;
        private Bitmap processedImage;
        private readonly BitmapModifier modifier;

        public FrmLoadDisplay()
        {
            InitializeComponent();
            bitmap = null;
            processedImage = null;
            imagePicker = new OpenFileDialog
            {
                Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)| *.jpg; *.jpeg; *.png; *.bmp"
            };
            imageSaver = new SaveFileDialog
            {
                Filter = "PNG Image|*.png|Bitmap Image|*.bmp"
            };
            modifier = new BitmapModifier();
            modifier.progressUpdater += UpdateProgress;
        }

        private void UpdateView()
        {
            // Display the source image
            pbxSourceImage.Image = bitmap;

            // Enable the buttons if we loaded an image
            bool areButtonsEnabled = bitmap != null;
            btnBicubic.Enabled = areButtonsEnabled;
            btnBilinear.Enabled = areButtonsEnabled;
            btnNearest.Enabled = areButtonsEnabled;

            // Enable save if a processed image is here
            tsmiSave.Enabled = processedImage != null;
        }

        private void TsmiLoad_Click(object sender, EventArgs e)
        {
            if (imagePicker.ShowDialog() == DialogResult.OK)
            {
                bitmap = (Bitmap)Image.FromFile(imagePicker.FileName);

                // Reset the processed image
                processedImage = null;

                // Reset the loading bar
                prbProcessing.Value = 0;

                UpdateView();
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UpdateProgress(int progress)
        {
            prbProcessing.Value = progress;
        }

        private void FrmLoadDisplay_Load(object sender, EventArgs e)
        {
            UpdateView();
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                btn.Enabled = false;
                string tag = btn.Tag.ToString();
                if (tag == TAG_REDIFY)
                {
                    long time = ExecutionMeasurer.Measure(modifier.Redify, bitmap, out processedImage);
                    tbxTimeOutput.Text = $"Redify : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                }
                else if (tag == TAG_GREYIFY_GET_SET)
                {
                    long time = ExecutionMeasurer.Measure(modifier.GreyifyGetSet, bitmap, out processedImage);
                    tbxTimeOutput.Text = $"GreyifyGetSet : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                }
                else if (tag == TAG_GREYIFY_LOCK_BITS)
                {
                    long time = ExecutionMeasurer.Measure(modifier.GreyifyLockBits, bitmap, out processedImage);
                    tbxTimeOutput.Text = $"GreyifyLockBits : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                }
                else if (tag == TAG_GREYIFY_UNSAFE)
                {
                    long time = ExecutionMeasurer.Measure(modifier.GreyifyPtr, bitmap, out processedImage);
                    tbxTimeOutput.Text = $"GreyifyPtr : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                }
                else if (tag == TAG_GREYIFY_PARALLEL)
                {
                    long time = ExecutionMeasurer.Measure(modifier.GreyifyPtrParallel, bitmap, out processedImage);
                    tbxTimeOutput.Text = $"GreyifyParallel : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                }
                else if (tag == TAG_MEDIAN)
                {
                    long time = ExecutionMeasurer.Measure(modifier.MedianFilter, bitmap, out processedImage);
                    tbxTimeOutput.Text = $"GreyifyParallel : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                }
                btn.Enabled = true;
            }

            UpdateView();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageSaver.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(imageSaver.FileName))
                {
                    File.Delete(imageSaver.FileName);
                }

                if (imageSaver.FilterIndex == 0)
                {
                    processedImage.Save(imageSaver.FileName, ImageFormat.Png);
                }
                else if (imageSaver.FilterIndex == 1)
                {
                    processedImage.Save(imageSaver.FileName, ImageFormat.Bmp);
                }
            }
        }
    }
}