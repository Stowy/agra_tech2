﻿using System.Diagnostics;
using System.Drawing;

namespace WF_Greyify
{
    internal static class ExecutionMeasurer
    {
        public delegate Bitmap ProcessImage(Bitmap image);

        /// <summary>
        /// Measure the execution time of the processImage delegate.
        /// </summary>
        /// <param name="processImage">Method to execute.</param>
        /// <param name="inImage">Parameter for the method.</param>
        /// <param name="outImage">Output of the method.</param>
        /// <returns>Returns the execution time of the method.</returns>
        public static long Measure(ProcessImage processImage, Bitmap inImage, out Bitmap outImage)
        {
            Stopwatch sw = Stopwatch.StartNew();
            outImage = processImage.Invoke(inImage);
            sw.Stop();
            return sw.ElapsedMilliseconds;
        }
    }
}