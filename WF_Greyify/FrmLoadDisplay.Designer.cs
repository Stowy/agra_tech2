﻿namespace WF_LoadDisplayImage
{
    partial class FrmLoadDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                bitmap?.Dispose();
                processedImage?.Dispose();
                imagePicker.Dispose();
                imageSaver.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbxSourceImage = new System.Windows.Forms.PictureBox();
            this.btnNearest = new System.Windows.Forms.Button();
            this.prbProcessing = new System.Windows.Forms.ProgressBar();
            this.btnBilinear = new System.Windows.Forms.Button();
            this.btnBicubic = new System.Windows.Forms.Button();
            this.tbxTimeOutput = new System.Windows.Forms.TextBox();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSourceImage)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(974, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLoad,
            this.toolStripMenuItem2,
            this.tsmiSave,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(37, 20);
            this.tsmiFile.Text = "File";
            // 
            // tsmiLoad
            // 
            this.tsmiLoad.Name = "tsmiLoad";
            this.tsmiLoad.Size = new System.Drawing.Size(147, 22);
            this.tsmiLoad.Text = "Load...";
            this.tsmiLoad.Click += new System.EventHandler(this.TsmiLoad_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(144, 6);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmiSave.Size = new System.Drawing.Size(147, 22);
            this.tsmiSave.Text = "Save...";
            this.tsmiSave.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // pbxSourceImage
            // 
            this.pbxSourceImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxSourceImage.Location = new System.Drawing.Point(219, 27);
            this.pbxSourceImage.Name = "pbxSourceImage";
            this.pbxSourceImage.Size = new System.Drawing.Size(370, 370);
            this.pbxSourceImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxSourceImage.TabIndex = 2;
            this.pbxSourceImage.TabStop = false;
            // 
            // btnNearest
            // 
            this.btnNearest.Location = new System.Drawing.Point(12, 27);
            this.btnNearest.Name = "btnNearest";
            this.btnNearest.Size = new System.Drawing.Size(201, 23);
            this.btnNearest.TabIndex = 1;
            this.btnNearest.Tag = "Nearest";
            this.btnNearest.Text = "Nearest neighbors";
            this.btnNearest.UseVisualStyleBackColor = true;
            this.btnNearest.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // prbProcessing
            // 
            this.prbProcessing.Location = new System.Drawing.Point(12, 374);
            this.prbProcessing.Name = "prbProcessing";
            this.prbProcessing.Size = new System.Drawing.Size(201, 23);
            this.prbProcessing.TabIndex = 4;
            // 
            // btnBilinear
            // 
            this.btnBilinear.Location = new System.Drawing.Point(12, 56);
            this.btnBilinear.Name = "btnBilinear";
            this.btnBilinear.Size = new System.Drawing.Size(201, 23);
            this.btnBilinear.TabIndex = 5;
            this.btnBilinear.Tag = "Bilinear";
            this.btnBilinear.Text = "Bilinear scaling";
            this.btnBilinear.UseVisualStyleBackColor = true;
            this.btnBilinear.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // btnBicubic
            // 
            this.btnBicubic.Location = new System.Drawing.Point(12, 85);
            this.btnBicubic.Name = "btnBicubic";
            this.btnBicubic.Size = new System.Drawing.Size(201, 23);
            this.btnBicubic.TabIndex = 6;
            this.btnBicubic.Tag = "Bicubic";
            this.btnBicubic.Text = "Bicubic";
            this.btnBicubic.UseVisualStyleBackColor = true;
            this.btnBicubic.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // tbxTimeOutput
            // 
            this.tbxTimeOutput.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTimeOutput.Location = new System.Drawing.Point(12, 114);
            this.tbxTimeOutput.Multiline = true;
            this.tbxTimeOutput.Name = "tbxTimeOutput";
            this.tbxTimeOutput.ReadOnly = true;
            this.tbxTimeOutput.Size = new System.Drawing.Size(201, 254);
            this.tbxTimeOutput.TabIndex = 9;
            // 
            // FrmLoadDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 405);
            this.Controls.Add(this.tbxTimeOutput);
            this.Controls.Add(this.btnBicubic);
            this.Controls.Add(this.btnBilinear);
            this.Controls.Add(this.prbProcessing);
            this.Controls.Add(this.btnNearest);
            this.Controls.Add(this.pbxSourceImage);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.MaximizeBox = false;
            this.Name = "FrmLoadDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Load and Display Image";
            this.Load += new System.EventHandler(this.FrmLoadDisplay_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSourceImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoad;
        private System.Windows.Forms.PictureBox pbxSourceImage;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btnNearest;
        private System.Windows.Forms.ProgressBar prbProcessing;
        private System.Windows.Forms.Button btnBilinear;
        private System.Windows.Forms.Button btnBicubic;
        private System.Windows.Forms.TextBox tbxTimeOutput;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsmiSave;
    }
}

