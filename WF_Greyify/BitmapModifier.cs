﻿namespace WF_LoadDisplayImage
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;
    using System.Threading.Tasks;

    public class BitmapModifier
    {
        public Action<int> progressUpdater;

        private const double RED_MODIFIER = 0.2989;
        private const double GREEN_MODIFIER = 0.5870;
        private const double BLUE_MODIFIER = 0.1140;

        private const int OFFSET_BLUE = 0;
        private const int OFFSET_GREEN = 1;
        private const int OFFSET_RED = 2;

        private readonly object imageLock;

        public BitmapModifier()
        {
            imageLock = new object();
        }

        /// <summary>
        /// Only keeps the red information on an image
        /// </summary>
        /// <param name="image">The image to modify.</param>
        /// <returns>Returns the modified image.</returns>
        public Bitmap Redify(Bitmap image)
        {
            Bitmap processedImage = new Bitmap(image);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    // Read the color data on the pixel
                    Color color = image.GetPixel(x, y);

                    // Keep only the red data
                    Color newColor = Color.FromArgb(color.R, 0, 0);

                    // Set the new color data on the output image
                    processedImage.SetPixel(x, y, newColor);
                }
                // Inform on the progress
                progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(x + 1) * 100 / image.Width)));
            }

            return processedImage;
        }

        /// <summary>
        /// Turns an image to greyscale with the Get/SetPixel method.
        /// </summary>
        public Bitmap GreyifyGetSet(Bitmap image)
        {
            Bitmap processedImage = new Bitmap(image);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    // Read the color data on the pixel
                    Color color = image.GetPixel(x, y);

                    // Greyify the pixel
                    int greyscaleColor = Convert.ToInt32(RED_MODIFIER * color.R + GREEN_MODIFIER * color.G + BLUE_MODIFIER * color.B);
                    Color newColor = Color.FromArgb(greyscaleColor, greyscaleColor, greyscaleColor);

                    // Set the new red data on the output image
                    processedImage.SetPixel(x, y, newColor);
                }
                // Inform on the progress
                progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(x + 1) * 100 / image.Width)));
            }

            return processedImage;
        }

        /// <summary>
        /// Turns an image to greyscale with the Lock Bits method.
        /// </summary>
        public Bitmap GreyifyLockBits(Bitmap image)
        {
            Bitmap outputImage = new Bitmap(image);
            // Lock the bits in memory
            BitmapData bitmapData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);

            // Gets the number of bytes in one pixel
            int bytesPerPixel = Image.GetPixelFormatSize(outputImage.PixelFormat) / 8;

            // Get the size in byte of the image with the stride
            int imageByteSize = bitmapData.Stride * outputImage.Height;

            // Copy the image in a byte array
            byte[] data = new byte[imageByteSize];
            Marshal.Copy(bitmapData.Scan0, data, 0, imageByteSize);

            for (int y = 0; y < bitmapData.Height; y++)
            {
                int currentLine = y * bitmapData.Stride;
                for (int x = 0; x < bitmapData.Width * bytesPerPixel; x += bytesPerPixel)
                {
                    int currentIndex = currentLine + x;
                    // Read the color data
                    int blue = data[currentIndex];
                    int green = data[currentIndex + 1];
                    int red = data[currentIndex + 2];

                    // Compute the greyscale color
                    byte greyscaleColor = (byte)Convert.ToInt32(RED_MODIFIER * red + GREEN_MODIFIER * green + BLUE_MODIFIER * blue);

                    // Write the greyscale color
                    data[currentIndex] = greyscaleColor;
                    data[currentIndex + 1] = greyscaleColor;
                    data[currentIndex + 2] = greyscaleColor;
                }
                // Inform on the progress
                progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(y + 1) * 100 / bitmapData.Height)));
            }

            // Copy the result in the bitmap
            Marshal.Copy(data, 0, bitmapData.Scan0, data.Length);

            // Unlocks the bits
            outputImage.UnlockBits(bitmapData);

            return outputImage;
        }

        /// <summary>
        /// Turns an image to greyscale with the Pointer method.
        /// </summary>
        public unsafe Bitmap GreyifyPtr(Bitmap image)
        {
            Bitmap outputImage = new Bitmap(image);

            // Lock the bits in memory
            BitmapData bitmapData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);

            // Gets the number of bytes in one pixel
            int bytesPerPixel = Image.GetPixelFormatSize(outputImage.PixelFormat) / 8;

            // Get the size in byte of the image with the stride
            int imageByteSize = bitmapData.Stride * outputImage.Height;

            lock (imageLock)
            {
                byte* startPtr = (byte*)bitmapData.Scan0.ToPointer();
                byte* currentPtr;
                for (int y = 0; y < bitmapData.Height; y++)
                {
                    int currentLine = y * bitmapData.Stride;
                    for (int x = 0; x < bitmapData.Width; x++)
                    {
                        int currentIndex = currentLine + x * bytesPerPixel;
                        currentPtr = startPtr + currentIndex;

                        // Compute the greyscale color
                        byte greyscaleColor = (byte)Convert.ToInt32(RED_MODIFIER * currentPtr[2] + GREEN_MODIFIER * currentPtr[1] + BLUE_MODIFIER * currentPtr[0]);

                        // Write the greyscale color
                        currentPtr[0] = greyscaleColor;
                        currentPtr[1] = greyscaleColor;
                        currentPtr[2] = greyscaleColor;
                    }
                    // Inform on the progress
                    progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(y + 1) * 100 / bitmapData.Height)));
                }
            }

            // Unlocks the bits
            outputImage.UnlockBits(bitmapData);

            return outputImage;
        }

        /// <summary>
        /// Turns an image to greyscale with the pointer and parallel method.
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public unsafe Bitmap GreyifyPtrParallel(Bitmap image)
        {
            Bitmap outputImage = new Bitmap(image);

            // Lock the bits in memory
            BitmapData bitmapData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);

            // Gets the number of bytes in one pixel
            int bytesPerPixel = Image.GetPixelFormatSize(outputImage.PixelFormat) / 8;

            // Get the size in byte of the image with the stride
            int imageByteSize = bitmapData.Stride * outputImage.Height;

            lock (imageLock)
            {
                byte* startPtr = (byte*)bitmapData.Scan0.ToPointer();
                Parallel.For(0, bitmapData.Height, y =>
                {
                    int currentLine = y * bitmapData.Stride;
                    byte* currentPtr;
                    for (int x = 0; x < bitmapData.Width; x++)
                    {
                        int currentIndex = currentLine + x * bytesPerPixel;
                        currentPtr = startPtr + currentIndex;

                        // Compute the greyscale color
                        byte greyscaleColor = (byte)Convert.ToInt32(RED_MODIFIER * currentPtr[2] + GREEN_MODIFIER * currentPtr[1] + BLUE_MODIFIER * currentPtr[0]);

                        // Write the greyscale color
                        currentPtr[0] = greyscaleColor;
                        currentPtr[1] = greyscaleColor;
                        currentPtr[2] = greyscaleColor;
                    }
                    // Inform on the progress
                    //progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(y + 1) * 100 / bitmapData.Height)));
                });
            }

            // Unlocks the bits
            outputImage.UnlockBits(bitmapData);

            return outputImage;
        }

        public unsafe Bitmap MedianFilter(Bitmap image)
        {
            Bitmap outputImage = new Bitmap(image);

            // Lock the bits in memory
            BitmapData bitmapData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);

            // Gets the number of bytes in one pixel
            int bytesPerPixel = Image.GetPixelFormatSize(outputImage.PixelFormat) / 8;

            // Get the size in byte of the image with the stride
            int imageByteSize = bitmapData.Stride * outputImage.Height;

            lock (imageLock)
            {
                byte* startPtr = (byte*)bitmapData.Scan0.ToPointer();
                for (int y = 0; y < bitmapData.Height; y++)
                {
                    for (int x = 0; x < bitmapData.Width; x++)
                    {
                        byte* currentPixel = startPtr + GetIndex(x, y, bitmapData.Stride, bytesPerPixel);

                        byte blueMiddle = GetMiddleValue(x, y, bitmapData.Height, bitmapData.Width, startPtr, bitmapData.Stride, bytesPerPixel, OFFSET_BLUE);
                        byte greenMiddle = GetMiddleValue(x, y, bitmapData.Height, bitmapData.Width, startPtr, bitmapData.Stride, bytesPerPixel, OFFSET_GREEN);
                        byte redMiddle = GetMiddleValue(x, y, bitmapData.Height, bitmapData.Width, startPtr, bitmapData.Stride, bytesPerPixel, OFFSET_RED);

                        currentPixel[OFFSET_BLUE] = blueMiddle;
                        currentPixel[OFFSET_GREEN] = greenMiddle;
                        currentPixel[OFFSET_RED] = redMiddle;
                    }
                    // Inform on the progress
                    progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(y + 1) * 100 / bitmapData.Height)));
                }
            }

            // Unlocks the bits
            outputImage.UnlockBits(bitmapData);

            return outputImage;
        }

        private static int GetIndex(int x, int y, int stride, int bytesPerPixel, int offset = 0)
        {
            int currentLine = y * stride;
            return currentLine + x * bytesPerPixel + offset;
        }

        private unsafe static byte GetMiddleValue(int x, int y, int height, int width, byte* startPtr, int stride, int bytesPerPixel, int colorOffset)
        {
            List<byte> neighbors = new List<byte>();
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (x + i >= 0 && x + i < width && y + j >= 0 && y + j < height)
                    {
                        byte* pixel = startPtr + GetIndex(x + i, y + j, stride, bytesPerPixel, colorOffset);
                        neighbors.Add(pixel[0]);
                    }
                }
            }
            neighbors.Sort();

            return neighbors[neighbors.Count / 2];
        }
    }
}