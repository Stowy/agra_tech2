﻿using System;
using System.Windows.Forms;
using WF_LoadDisplayImage;

namespace WF_Greyify
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmLoadDisplay());
        }
    }
}