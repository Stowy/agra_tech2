﻿// <copyright file="BitmapResizer.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_Resize
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Windows.Forms;

    /// <summary>
    /// Class that contains methods to resize an image.
    /// </summary>
    public class BitmapResizer
    {
        private const int OffsetBlue = 0;
        private const int OffsetGreen = 1;
        private const int OffsetRed = 2;

        private readonly object imageLock;

        /// <summary>
        /// Initializes a new instance of the <see cref="BitmapResizer"/> class.
        /// </summary>
        public BitmapResizer()
        {
            imageLock = new object();
        }

        /// <summary>
        /// Gets or sets the progress updater.
        /// </summary>
        public Action<int> ProgressUpdater { get; set; }

        /// <summary>
        /// Scales the image with the nearest neighbors algorithm.
        /// </summary>
        /// <param name="inputBitmap">Image to scale.</param>
        /// <param name="scale">Scale to modify the image to.</param>
        /// <returns>Returns the scaled image.</returns>
        public unsafe Bitmap ScaleNearestNeighbors(Bitmap inputBitmap, float scale)
        {
            int width = Convert.ToInt32(Math.Floor(inputBitmap.Width * scale));
            int height = Convert.ToInt32(Math.Floor(inputBitmap.Height * scale));
            float ratio = 1 / scale;
            Bitmap outputBitmap = new Bitmap(width, height, inputBitmap.PixelFormat);

            // Lock the bits in memory for the output image
            BitmapData outputBitmapData = outputBitmap.LockBits(
                new Rectangle(0, 0, outputBitmap.Width, outputBitmap.Height),
                ImageLockMode.ReadWrite,
                outputBitmap.PixelFormat);

            // Lock the bits in memory for the input image
            BitmapData inputBitmapData = inputBitmap.LockBits(
                new Rectangle(0, 0, inputBitmap.Width, inputBitmap.Height),
                ImageLockMode.ReadWrite,
                inputBitmap.PixelFormat);

            // Gets the number of bytes in one pixel
            int bytesPerPixel = Image.GetPixelFormatSize(inputBitmap.PixelFormat) / 8;

            // Get the size in byte of the image with the stride
            int imageByteSize = outputBitmapData.Stride * inputBitmap.Height;

            lock (imageLock)
            {
                // Gets the start pointer of the output and input image.
                byte* outputStartPtr = (byte*)outputBitmapData.Scan0.ToPointer();
                byte* inputStartPtr = (byte*)inputBitmapData.Scan0.ToPointer();

                for (int yOutput = 0; yOutput < outputBitmapData.Height; yOutput++)
                {
                    for (int xOutput = 0; xOutput < outputBitmapData.Width; xOutput++)
                    {
                        int xInput = Convert.ToInt32(Math.Floor(xOutput * ratio));
                        int yInput = Convert.ToInt32(Math.Floor(yOutput * ratio));

                        // Reads the data in the input image
                        byte* currentPixelInput = inputStartPtr + GetIndex(xInput, yInput, inputBitmapData.Stride, bytesPerPixel);
                        byte red = currentPixelInput[OffsetRed];
                        byte green = currentPixelInput[OffsetGreen];
                        byte blue = currentPixelInput[OffsetBlue];

                        // Writes the data in the ouput image
                        byte* currentPixelOutput = outputStartPtr + GetIndex(xOutput, yOutput, outputBitmapData.Stride, bytesPerPixel);
                        currentPixelOutput[OffsetRed] = red;
                        currentPixelOutput[OffsetGreen] = green;
                        currentPixelOutput[OffsetBlue] = blue;
                    }

                    // Informs on the progress
                    ProgressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(yOutput + 1) * 100 / outputBitmapData.Height)));
                }
            }

            // Unlocks the bits
            outputBitmap.UnlockBits(outputBitmapData);
            inputBitmap.UnlockBits(inputBitmapData);

            return outputBitmap;
        }

        /// <summary>
        /// Scales the image with the bilinear algorithm.
        /// </summary>
        /// <param name="inputBitmap">Image to scale.</param>
        /// <param name="scale">Scale to modify the image to.</param>
        /// <returns>Returns the scaled image.</returns>
        public unsafe Bitmap ScaleBilinear(Bitmap inputBitmap, float scale)
        {
            int width = Convert.ToInt32(Math.Floor(inputBitmap.Width * scale));
            int height = Convert.ToInt32(Math.Floor(inputBitmap.Height * scale));
            float ratio = 1 / scale;
            Bitmap outputBitmap = new Bitmap(width, height, inputBitmap.PixelFormat);

            // Lock the bits in memory for the output image
            BitmapData outputBitmapData = outputBitmap.LockBits(
                new Rectangle(0, 0, outputBitmap.Width, outputBitmap.Height),
                ImageLockMode.ReadWrite,
                outputBitmap.PixelFormat);

            // Lock the bits in memory for the input image
            BitmapData inputBitmapData = inputBitmap.LockBits(
                new Rectangle(0, 0, inputBitmap.Width, inputBitmap.Height),
                ImageLockMode.ReadWrite,
                inputBitmap.PixelFormat);

            // Gets the number of bytes in one pixel
            int bytesPerPixel = Image.GetPixelFormatSize(inputBitmap.PixelFormat) / 8;

            // Get the size in byte of the image with the stride
            int imageByteSize = outputBitmapData.Stride * inputBitmap.Height;

            lock (imageLock)
            {
                // Gets the start pointer of the output and input image.
                byte* outputStartPtr = (byte*)outputBitmapData.Scan0.ToPointer();
                byte* inputStartPtr = (byte*)inputBitmapData.Scan0.ToPointer();

                for (int yOutput = 0; yOutput < outputBitmapData.Height; yOutput++)
                {
                    for (int xOutput = 0; xOutput < outputBitmapData.Width; xOutput++)
                    {
                        int xInput = Convert.ToInt32(Math.Floor(xOutput * ratio));
                        int yInput = Convert.ToInt32(Math.Floor(yOutput * ratio));

                        float xDiff = ((float)xOutput * ratio) - xInput;
                        float yDiff = ((float)yOutput * ratio) - yInput;

                        Color outputColor = ComputeBilinearPixel(
                            inputStartPtr,
                            xInput,
                            yInput,
                            xDiff,
                            yDiff,
                            inputBitmapData.Stride,
                            bytesPerPixel,
                            inputBitmapData.Height,
                            inputBitmapData.Width);

                        // Write the data in the ouput image
                        byte* currentPixelOutput = outputStartPtr + GetIndex(xOutput, yOutput, outputBitmapData.Stride, bytesPerPixel);
                        currentPixelOutput[OffsetRed] = outputColor.R;
                        currentPixelOutput[OffsetGreen] = outputColor.G;
                        currentPixelOutput[OffsetBlue] = outputColor.B;
                    }

                    // Informs on the progress
                    ProgressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(yOutput + 1) * 100 / outputBitmapData.Height)));
                }
            }

            // Unlocks the bits
            outputBitmap.UnlockBits(outputBitmapData);
            inputBitmap.UnlockBits(inputBitmapData);

            return outputBitmap;
        }

        private static int GetIndex(int x, int y, int stride, int bytesPerPixel, int offset = 0)
        {
            int currentLine = y * stride;
            return currentLine + (x * bytesPerPixel) + offset;
        }

        private static unsafe Color ComputeBilinearPixel(
            byte* startPtr,
            int xInput,
            int yInput,
            float xDiff,
            float yDiff,
            int stride,
            int bytesPerPixel,
            int height,
            int width)
        {
            byte* currentPixelInput = startPtr + GetIndex(xInput, yInput, stride, bytesPerPixel);
            byte* neighborPixel1 = null;
            byte* neighborPixel2 = null;
            byte* neighborPixel3 = null;

            if (yInput + 1 < height)
            {
                neighborPixel1 = startPtr + GetIndex(xInput, yInput + 1, stride, bytesPerPixel);
            }

            if (xInput + 1 < width)
            {
                neighborPixel2 = startPtr + GetIndex(xInput + 1, yInput, stride, bytesPerPixel);
            }

            if (yInput + 1 < height && xInput + 1 < width)
            {
                neighborPixel3 = startPtr + GetIndex(xInput + 1, yInput + 1, stride, bytesPerPixel);
            }

            byte red = ComputeBilinearColor(currentPixelInput, neighborPixel1, neighborPixel2, neighborPixel3, xDiff, yDiff, OffsetRed);
            byte green = ComputeBilinearColor(currentPixelInput, neighborPixel1, neighborPixel2, neighborPixel3, xDiff, yDiff, OffsetGreen);
            byte blue = ComputeBilinearColor(currentPixelInput, neighborPixel1, neighborPixel2, neighborPixel3, xDiff, yDiff, OffsetBlue);

            return Color.FromArgb(red, green, blue);
        }

        private static unsafe byte ComputeBilinearColor(
            byte* currentPixelInput,
            byte* neighborPixel1,
            byte* neighborPixel2,
            byte* neighborPixel3,
            float xDiff,
            float yDiff,
            int offset = 0)
        {
            byte currentPixelValue = currentPixelInput[offset];
            byte color = Convert.ToByte(currentPixelValue * (1 - xDiff) * (1 - yDiff));
            if (neighborPixel1 != null)
            {
                color += Convert.ToByte(neighborPixel1[offset] * (1 - xDiff) * yDiff);
            }
            else
            {
                color += Convert.ToByte(currentPixelValue * (1 - xDiff) * yDiff);
            }

            if (neighborPixel2 != null)
            {
                color += Convert.ToByte(neighborPixel2[offset] * xDiff * (1 - yDiff));
            }
            else
            {
                color += Convert.ToByte(currentPixelValue * xDiff * (1 - yDiff));
            }

            if (neighborPixel3 != null)
            {
                color += Convert.ToByte(neighborPixel3[offset] * yDiff * xDiff);
            }
            else
            {
                color += Convert.ToByte(currentPixelValue * yDiff * xDiff);
            }

            return color;
        }
    }
}