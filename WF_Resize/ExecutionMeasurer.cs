﻿// <copyright file="ExecutionMeasurer.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_Resize
{
    using System.Diagnostics;
    using System.Drawing;

    /// <summary>
    /// Measures the execution of the methods in BitmapResizer.
    /// </summary>
    public static class ExecutionMeasurer
    {
        /// <summary>
        /// Delegare of the methods in BitmapResizer.
        /// </summary>
        /// <param name="image">Image to resize.</param>
        /// <param name="scale">Scale to resize the image to.</param>
        /// <returns>Returns the resized image.</returns>
        public delegate Bitmap ProcessImage(Bitmap image, float scale);

        /// <summary>
        /// Measure the execution time of the processImage delegate.
        /// </summary>
        /// <param name="processImage">Method to execute.</param>
        /// <param name="inImage">Image parameter for the method.</param>
        /// <param name="scale">Scale parameter for the method.</param>
        /// <param name="outImage">Output of the method.</param>
        /// <returns>Returns the execution time of the method.</returns>
        public static long Measure(ProcessImage processImage, Bitmap inImage, float scale, out Bitmap outImage)
        {
            Stopwatch sw = Stopwatch.StartNew();
            outImage = processImage.Invoke(inImage, scale);
            sw.Stop();
            return sw.ElapsedMilliseconds;
        }
    }
}