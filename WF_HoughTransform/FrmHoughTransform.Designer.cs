﻿namespace WF_HoughTransform
{
    partial class FrmHoughTransform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbxEdges = new System.Windows.Forms.PictureBox();
            this.pbxLines = new System.Windows.Forms.PictureBox();
            this.trbTreshold = new System.Windows.Forms.TrackBar();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxEdges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbTreshold)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1127, 24);
            this.mainMenuStrip.TabIndex = 2;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLoad,
            this.toolStripMenuItem2,
            this.tsmiSave,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(37, 20);
            this.tsmiFile.Text = "File";
            // 
            // tsmiLoad
            // 
            this.tsmiLoad.Name = "tsmiLoad";
            this.tsmiLoad.Size = new System.Drawing.Size(147, 22);
            this.tsmiLoad.Text = "Load...";
            this.tsmiLoad.Click += new System.EventHandler(this.TsmiLoad_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(144, 6);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmiSave.Size = new System.Drawing.Size(147, 22);
            this.tsmiSave.Text = "Save...";
            this.tsmiSave.Click += new System.EventHandler(this.TsmiSave_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // pbxEdges
            // 
            this.pbxEdges.Location = new System.Drawing.Point(12, 75);
            this.pbxEdges.Name = "pbxEdges";
            this.pbxEdges.Size = new System.Drawing.Size(550, 550);
            this.pbxEdges.TabIndex = 3;
            this.pbxEdges.TabStop = false;
            // 
            // pbxLines
            // 
            this.pbxLines.Location = new System.Drawing.Point(568, 74);
            this.pbxLines.Name = "pbxLines";
            this.pbxLines.Size = new System.Drawing.Size(550, 550);
            this.pbxLines.TabIndex = 4;
            this.pbxLines.TabStop = false;
            // 
            // trbTreshold
            // 
            this.trbTreshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.trbTreshold.Location = new System.Drawing.Point(0, 24);
            this.trbTreshold.Maximum = 1000;
            this.trbTreshold.Name = "trbTreshold";
            this.trbTreshold.Size = new System.Drawing.Size(1127, 45);
            this.trbTreshold.TabIndex = 5;
            this.trbTreshold.Value = 500;
            this.trbTreshold.Scroll += new System.EventHandler(this.TrbTreshold_Scroll);
            // 
            // FrmHoughTransform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 636);
            this.Controls.Add(this.trbTreshold);
            this.Controls.Add(this.pbxLines);
            this.Controls.Add(this.pbxEdges);
            this.Controls.Add(this.mainMenuStrip);
            this.Name = "FrmHoughTransform";
            this.Text = "Hough Transform";
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxEdges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbTreshold)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoad;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsmiSave;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbxEdges;
        private System.Windows.Forms.PictureBox pbxLines;
        private System.Windows.Forms.TrackBar trbTreshold;
    }
}

