﻿// <copyright file="FrmHoughTransform.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_HoughTransform
{
    using System;
    using System.Data;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Forms;
    using OpenCvSharp;
    using OpenCvSharp.Extensions;

    /// <summary>
    /// Main form of the hough transform project.
    /// </summary>
    public partial class FrmHoughTransform : Form
    {
        private readonly OpenFileDialog imagePicker;
        private readonly SaveFileDialog imageSaver;
        private Mat inputMat;
        private Mat edges;
        private Mat outputMat;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmHoughTransform"/> class.
        /// </summary>
        public FrmHoughTransform()
        {
            this.InitializeComponent();

            this.imagePicker = new OpenFileDialog
            {
                Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)| *.jpg; *.jpeg; *.png; *.bmp",
            };

            this.imageSaver = new SaveFileDialog
            {
                Filter = "PNG Image|*.png|Bitmap Image|*.bmp",
            };

            this.inputMat = null;
            this.edges = null;
            this.outputMat = null;
        }

        private void TsmiLoad_Click(object sender, System.EventArgs e)
        {
            if (this.imagePicker.ShowDialog() == DialogResult.OK)
            {
                this.inputMat = Cv2.ImRead(this.imagePicker.FileName, ImreadModes.Grayscale).Resize(new Size(this.pbxEdges.Width, this.pbxEdges.Height));

                this.ComputeHough();
            }
        }

        private void ComputeHough()
        {
            this.edges = new Mat();

            // Find the edges
            double treshold = this.trbTreshold.Value;
            double treshold2 = treshold * 0.4;
            Cv2.Canny(this.inputMat, this.edges, treshold, treshold2);

            // Compute hough transform
            LineSegmentPoint[] segHough = Cv2.HoughLinesP(this.edges, 1, Math.PI / 180, 100, 100, 10);

            // Draw the hough transform on the output image
            this.outputMat = this.inputMat.EmptyClone();
            foreach (LineSegmentPoint lineSegmentPoint in segHough)
            {
                this.outputMat.Line(lineSegmentPoint.P1, lineSegmentPoint.P2, Scalar.White, 1, LineTypes.AntiAlias, 0);
            }

            // Update the view with the new images
            this.UpdateView();
        }

        /// <summary>
        /// Updates the view.
        /// </summary>
        private void UpdateView()
        {
            this.pbxEdges.Image = this.edges.ToBitmap();
            this.pbxLines.Image = this.outputMat.ToBitmap();
        }

        private void TsmiSave_Click(object sender, EventArgs e)
        {
            if (this.outputMat == null)
            {
                return;
            }

            if (this.imageSaver.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(this.imageSaver.FileName))
                {
                    File.Delete(this.imageSaver.FileName);
                }

                if (this.imageSaver.FilterIndex == 0)
                {
                    this.outputMat.ToBitmap().Save(this.imageSaver.FileName, ImageFormat.Png);
                }
                else if (this.imageSaver.FilterIndex == 1)
                {
                    this.outputMat.ToBitmap().Save(this.imageSaver.FileName, ImageFormat.Bmp);
                }
            }
        }

        private void TrbTreshold_Scroll(object sender, EventArgs e)
        {
            this.ComputeHough();
        }
    }
}
