﻿using System;
using System.Drawing;

namespace WF_LoadDisplayImage
{
    public class BitmapModifier
    {
        public Action<int> progressUpdater;

        private const int DEFAULT_INCREMENT = 50;

        public Bitmap IncrementReds(Bitmap image, int value = DEFAULT_INCREMENT)
        {
            Bitmap processedImage = new Bitmap(image);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    // Read the color data on the pixel
                    Color color = image.GetPixel(x, y);

                    // Increment the red data
                    int newRed = (color.R + value) % 255;
                    Color newColor = Color.FromArgb(newRed, color.G, color.B);

                    // Set the new red data on the output image
                    processedImage.SetPixel(x, y, newColor);
                }
                // Inform on the progress
                progressUpdater?.Invoke(Convert.ToInt32(Math.Ceiling((decimal)(x + 1) * 100 / image.Width)));
            }

            return processedImage;
        }
    }
}