﻿namespace WF_LoadDisplayImage
{
    partial class FrmLoadDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                bitmap?.Dispose();
                processedImage?.Dispose();
                imagePicker.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbxSourceImage = new System.Windows.Forms.PictureBox();
            this.pbxProcessedImage = new System.Windows.Forms.PictureBox();
            this.btnExecuteProcessing = new System.Windows.Forms.Button();
            this.nudProcessingValue = new System.Windows.Forms.NumericUpDown();
            this.prbProcessing = new System.Windows.Forms.ProgressBar();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSourceImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProcessedImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudProcessingValue)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1029, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLoad,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(37, 20);
            this.tsmiFile.Text = "File";
            // 
            // tsmiLoad
            // 
            this.tsmiLoad.Name = "tsmiLoad";
            this.tsmiLoad.Size = new System.Drawing.Size(135, 22);
            this.tsmiLoad.Text = "Load...";
            this.tsmiLoad.Click += new System.EventHandler(this.TsmiLoad_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(132, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // pbxSourceImage
            // 
            this.pbxSourceImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxSourceImage.Location = new System.Drawing.Point(12, 27);
            this.pbxSourceImage.Name = "pbxSourceImage";
            this.pbxSourceImage.Size = new System.Drawing.Size(500, 500);
            this.pbxSourceImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSourceImage.TabIndex = 2;
            this.pbxSourceImage.TabStop = false;
            // 
            // pbxProcessedImage
            // 
            this.pbxProcessedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxProcessedImage.Location = new System.Drawing.Point(518, 27);
            this.pbxProcessedImage.Name = "pbxProcessedImage";
            this.pbxProcessedImage.Size = new System.Drawing.Size(500, 500);
            this.pbxProcessedImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxProcessedImage.TabIndex = 3;
            this.pbxProcessedImage.TabStop = false;
            // 
            // btnExecuteProcessing
            // 
            this.btnExecuteProcessing.Location = new System.Drawing.Point(12, 539);
            this.btnExecuteProcessing.Name = "btnExecuteProcessing";
            this.btnExecuteProcessing.Size = new System.Drawing.Size(75, 23);
            this.btnExecuteProcessing.TabIndex = 1;
            this.btnExecuteProcessing.Text = "Process";
            this.btnExecuteProcessing.UseVisualStyleBackColor = true;
            this.btnExecuteProcessing.Click += new System.EventHandler(this.BtnExecuteProcessing_Click);
            // 
            // nudProcessingValue
            // 
            this.nudProcessingValue.Location = new System.Drawing.Point(93, 542);
            this.nudProcessingValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudProcessingValue.Name = "nudProcessingValue";
            this.nudProcessingValue.Size = new System.Drawing.Size(120, 20);
            this.nudProcessingValue.TabIndex = 2;
            this.nudProcessingValue.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // prbProcessing
            // 
            this.prbProcessing.Location = new System.Drawing.Point(12, 568);
            this.prbProcessing.Name = "prbProcessing";
            this.prbProcessing.Size = new System.Drawing.Size(201, 23);
            this.prbProcessing.TabIndex = 4;
            // 
            // FrmLoadDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 600);
            this.Controls.Add(this.prbProcessing);
            this.Controls.Add(this.nudProcessingValue);
            this.Controls.Add(this.btnExecuteProcessing);
            this.Controls.Add(this.pbxProcessedImage);
            this.Controls.Add(this.pbxSourceImage);
            this.Controls.Add(this.mainMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenuStrip;
            this.MaximizeBox = false;
            this.Name = "FrmLoadDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Load and Display Image";
            this.Load += new System.EventHandler(this.FrmLoadDisplay_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSourceImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProcessedImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudProcessingValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoad;
        private System.Windows.Forms.PictureBox pbxSourceImage;
        private System.Windows.Forms.PictureBox pbxProcessedImage;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btnExecuteProcessing;
        private System.Windows.Forms.NumericUpDown nudProcessingValue;
        private System.Windows.Forms.ProgressBar prbProcessing;
    }
}

