﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WF_LoadDisplayImage
{
    public partial class FrmLoadDisplay : Form
    {
        private readonly OpenFileDialog imagePicker;
        private Bitmap bitmap;
        private Bitmap processedImage;
        private readonly BitmapModifier modifier;

        public FrmLoadDisplay()
        {
            InitializeComponent();
            bitmap = null;
            processedImage = null;
            imagePicker = new OpenFileDialog
            {
                Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)| *.jpg; *.jpeg; *.png; *.bmp"
            };
            modifier = new BitmapModifier();
            modifier.progressUpdater += UpdateProgress;
        }

        private void UpdateView()
        {
            // Display the source image
            pbxSourceImage.Image = bitmap;

            // Display processed image
            pbxProcessedImage.Image = processedImage;

            // Enable the button and it's numeric up down if we loaded an image
            btnExecuteProcessing.Enabled = bitmap != null;
            nudProcessingValue.Enabled = bitmap != null;
        }

        private void TsmiLoad_Click(object sender, EventArgs e)
        {
            if (imagePicker.ShowDialog() == DialogResult.OK)
            {
                bitmap = (Bitmap)Image.FromFile(imagePicker.FileName);

                // Reset the processed image
                processedImage = null;

                // Reset the loading bar
                prbProcessing.Value = 0;

                UpdateView();
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnExecuteProcessing_Click(object sender, EventArgs e)
        {
            btnExecuteProcessing.Enabled = false;
            nudProcessingValue.Enabled = false;

            processedImage = modifier.IncrementReds(bitmap, Convert.ToInt32(nudProcessingValue.Value));

            btnExecuteProcessing.Enabled = true;
            nudProcessingValue.Enabled = true;

            UpdateView();
        }

        private void UpdateProgress(int progress)
        {
            prbProcessing.Value = progress;
        }

        private void FrmLoadDisplay_Load(object sender, EventArgs e)
        {
            UpdateView();
        }
    }
}