﻿namespace WF_FilterOperation
{
    partial class FrmFilterOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                inputBitmap?.Dispose();
                imagePicker.Dispose();
                imageSaver.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbxSourceImage = new System.Windows.Forms.PictureBox();
            this.prbProcessing = new System.Windows.Forms.ProgressBar();
            this.tbxTimeOutput = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rdbAnd = new System.Windows.Forms.RadioButton();
            this.rdbOr = new System.Windows.Forms.RadioButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.rdbAdd = new System.Windows.Forms.RadioButton();
            this.btnLoadFirstImage = new System.Windows.Forms.Button();
            this.btnLoadSecondImage = new System.Windows.Forms.Button();
            this.btnComputeFinalImage = new System.Windows.Forms.Button();
            this.rdbSubstract = new System.Windows.Forms.RadioButton();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSourceImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1032, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLoad,
            this.toolStripMenuItem2,
            this.tsmiSave,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(37, 20);
            this.tsmiFile.Text = "File";
            // 
            // tsmiLoad
            // 
            this.tsmiLoad.Name = "tsmiLoad";
            this.tsmiLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.tsmiLoad.Size = new System.Drawing.Size(180, 22);
            this.tsmiLoad.Text = "Load...";
            this.tsmiLoad.Click += new System.EventHandler(this.TsmiLoad_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(177, 6);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmiSave.Size = new System.Drawing.Size(180, 22);
            this.tsmiSave.Text = "Save...";
            this.tsmiSave.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // pbxSourceImage
            // 
            this.pbxSourceImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxSourceImage.Location = new System.Drawing.Point(219, 27);
            this.pbxSourceImage.Name = "pbxSourceImage";
            this.pbxSourceImage.Size = new System.Drawing.Size(250, 250);
            this.pbxSourceImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxSourceImage.TabIndex = 2;
            this.pbxSourceImage.TabStop = false;
            // 
            // prbProcessing
            // 
            this.prbProcessing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prbProcessing.Location = new System.Drawing.Point(12, 424);
            this.prbProcessing.Name = "prbProcessing";
            this.prbProcessing.Size = new System.Drawing.Size(201, 23);
            this.prbProcessing.TabIndex = 4;
            // 
            // tbxTimeOutput
            // 
            this.tbxTimeOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxTimeOutput.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTimeOutput.Location = new System.Drawing.Point(12, 164);
            this.tbxTimeOutput.Multiline = true;
            this.tbxTimeOutput.Name = "tbxTimeOutput";
            this.tbxTimeOutput.ReadOnly = true;
            this.tbxTimeOutput.Size = new System.Drawing.Size(201, 254);
            this.tbxTimeOutput.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(475, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 250);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // rdbAnd
            // 
            this.rdbAnd.AutoSize = true;
            this.rdbAnd.Checked = true;
            this.rdbAnd.Location = new System.Drawing.Point(12, 27);
            this.rdbAnd.Name = "rdbAnd";
            this.rdbAnd.Size = new System.Drawing.Size(35, 17);
            this.rdbAnd.TabIndex = 11;
            this.rdbAnd.TabStop = true;
            this.rdbAnd.Text = "Et";
            this.rdbAnd.UseVisualStyleBackColor = true;
            // 
            // rdbOr
            // 
            this.rdbOr.AutoSize = true;
            this.rdbOr.Location = new System.Drawing.Point(12, 50);
            this.rdbOr.Name = "rdbOr";
            this.rdbOr.Size = new System.Drawing.Size(39, 17);
            this.rdbOr.TabIndex = 12;
            this.rdbOr.Text = "Ou";
            this.rdbOr.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(731, 27);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(250, 250);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // rdbAdd
            // 
            this.rdbAdd.AutoSize = true;
            this.rdbAdd.Location = new System.Drawing.Point(12, 73);
            this.rdbAdd.Name = "rdbAdd";
            this.rdbAdd.Size = new System.Drawing.Size(63, 17);
            this.rdbAdd.TabIndex = 14;
            this.rdbAdd.Text = "Addition";
            this.rdbAdd.UseVisualStyleBackColor = true;
            // 
            // btnLoadFirstImage
            // 
            this.btnLoadFirstImage.Location = new System.Drawing.Point(219, 283);
            this.btnLoadFirstImage.Name = "btnLoadFirstImage";
            this.btnLoadFirstImage.Size = new System.Drawing.Size(250, 23);
            this.btnLoadFirstImage.TabIndex = 15;
            this.btnLoadFirstImage.Text = "Load first image";
            this.btnLoadFirstImage.UseVisualStyleBackColor = true;
            // 
            // btnLoadSecondImage
            // 
            this.btnLoadSecondImage.Location = new System.Drawing.Point(475, 283);
            this.btnLoadSecondImage.Name = "btnLoadSecondImage";
            this.btnLoadSecondImage.Size = new System.Drawing.Size(250, 23);
            this.btnLoadSecondImage.TabIndex = 16;
            this.btnLoadSecondImage.Text = "Load second image";
            this.btnLoadSecondImage.UseVisualStyleBackColor = true;
            // 
            // btnComputeFinalImage
            // 
            this.btnComputeFinalImage.Location = new System.Drawing.Point(731, 283);
            this.btnComputeFinalImage.Name = "btnComputeFinalImage";
            this.btnComputeFinalImage.Size = new System.Drawing.Size(250, 23);
            this.btnComputeFinalImage.TabIndex = 17;
            this.btnComputeFinalImage.Text = "Compute final image";
            this.btnComputeFinalImage.UseVisualStyleBackColor = true;
            // 
            // rdbSubstract
            // 
            this.rdbSubstract.AutoSize = true;
            this.rdbSubstract.Location = new System.Drawing.Point(13, 96);
            this.rdbSubstract.Name = "rdbSubstract";
            this.rdbSubstract.Size = new System.Drawing.Size(84, 17);
            this.rdbSubstract.TabIndex = 18;
            this.rdbSubstract.Text = "Soustraction";
            this.rdbSubstract.UseVisualStyleBackColor = true;
            // 
            // FrmFilterOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 456);
            this.Controls.Add(this.rdbSubstract);
            this.Controls.Add(this.btnComputeFinalImage);
            this.Controls.Add(this.btnLoadSecondImage);
            this.Controls.Add(this.btnLoadFirstImage);
            this.Controls.Add(this.rdbAdd);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.rdbOr);
            this.Controls.Add(this.rdbAnd);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tbxTimeOutput);
            this.Controls.Add(this.prbProcessing);
            this.Controls.Add(this.pbxSourceImage);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "FrmFilterOperation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resize";
            this.Load += new System.EventHandler(this.FrmLoadDisplay_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSourceImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoad;
        private System.Windows.Forms.PictureBox pbxSourceImage;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ProgressBar prbProcessing;
        private System.Windows.Forms.TextBox tbxTimeOutput;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsmiSave;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rdbAnd;
        private System.Windows.Forms.RadioButton rdbOr;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RadioButton rdbAdd;
        private System.Windows.Forms.Button btnLoadFirstImage;
        private System.Windows.Forms.Button btnLoadSecondImage;
        private System.Windows.Forms.Button btnComputeFinalImage;
        private System.Windows.Forms.RadioButton rdbSubstract;
    }
}

