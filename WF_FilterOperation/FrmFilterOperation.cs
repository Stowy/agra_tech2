﻿// <copyright file="FrmFilterOperation.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_FilterOperation
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// Form for resising images.
    /// </summary>
    public partial class FrmFilterOperation : Form
    {
        private readonly OpenFileDialog imagePicker;
        private readonly SaveFileDialog imageSaver;

        // private readonly BitmapResizer resizer;
        private readonly ToolStripMenuItem[] scaleItems;
        private Bitmap inputBitmap;
        private Bitmap outputBitmap;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmFilterOperation"/> class.
        /// </summary>
        public FrmFilterOperation()
        {
            InitializeComponent();
            inputBitmap = null;
            outputBitmap = null;
            imagePicker = new OpenFileDialog
            {
                Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)| *.jpg; *.jpeg; *.png; *.bmp",
            };
            imageSaver = new SaveFileDialog
            {
                Filter = "PNG Image|*.png|Bitmap Image|*.bmp",
            };

            // resizer = new BitmapResizer();
            // resizer.ProgressUpdater += UpdateProgress;
            scaleItems = new ToolStripMenuItem[] { tsmi12Scale, tsmi1Scale, tsmi2Scale, tsmi4Scale, tsmi6Scale };
        }

        private void UpdateView()
        {
            // Display the source image
            pbxSourceImage.Image = outputBitmap ?? inputBitmap;

            // Enable the buttons if we loaded an image
            bool areButtonsEnabled = inputBitmap != null;
            rdbNearest.Enabled = areButtonsEnabled;
            rdbBilinear.Enabled = areButtonsEnabled;
            rdbBicubic.Enabled = areButtonsEnabled;

            // Enable save if an image is here
            tsmiSave.Enabled = areButtonsEnabled;

            foreach (ToolStripMenuItem item in scaleItems)
            {
                item.Enabled = areButtonsEnabled;
            }
        }

        private void TsmiLoad_Click(object sender, EventArgs e)
        {
            if (imagePicker.ShowDialog() == DialogResult.OK)
            {
                inputBitmap = (Bitmap)Image.FromFile(imagePicker.FileName);
                outputBitmap = null;

                // Reset the loading bar
                prbProcessing.Value = 0;

                // Uncheck all tsmi
                foreach (ToolStripMenuItem item in scaleItems)
                {
                    item.Checked = false;
                }

                tsmi1Scale.Checked = true;

                UpdateView();
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UpdateProgress(int progress)
        {
            prbProcessing.Value = progress;
        }

        private void FrmLoadDisplay_Load(object sender, EventArgs e)
        {
            UpdateView();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (outputBitmap == null)
            {
                return;
            }

            if (imageSaver.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(imageSaver.FileName))
                {
                    File.Delete(imageSaver.FileName);
                }

                if (imageSaver.FilterIndex == 0)
                {
                    outputBitmap.Save(imageSaver.FileName, ImageFormat.Png);
                }
                else if (imageSaver.FilterIndex == 1)
                {
                    outputBitmap.Save(imageSaver.FileName, ImageFormat.Bmp);
                }
            }
        }

        private void ScaleFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Uncheck other ToolStripMenuItems
            foreach (ToolStripMenuItem item in scaleItems)
            {
                if (item != sender)
                {
                    item.Checked = false;
                }
            }

            if (sender is ToolStripMenuItem toolStripItem)
            {
                // Disable all scale buttons
                foreach (ToolStripMenuItem item in scaleItems)
                {
                    item.Enabled = false;
                }

                // Disable all radio buttons
                rdbNearest.Enabled = false;
                rdbBilinear.Enabled = false;
                rdbBicubic.Enabled = false;

                float scale = Convert.ToSingle(toolStripItem.Tag);
                if (scale == 1)
                {
                    outputBitmap = null;
                    UpdateView();
                    return;
                }

                // if (rdbNearest.Checked)
                // {
                //     long time = ExecutionMeasurer.Measure(resizer.ScaleNearestNeighbors, inputBitmap, scale, out outputBitmap);
                //     tbxTimeOutput.Text = $"Resize Nearest : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                // }
                // else if (rdbBilinear.Checked)
                // {
                //     long time = ExecutionMeasurer.Measure(resizer.ScaleBilinear, inputBitmap, scale, out outputBitmap);
                //     tbxTimeOutput.Text = $"Resize Bilinear : {time}{Environment.NewLine}{tbxTimeOutput.Text}";
                // }

                // Enable all scale buttons
                foreach (ToolStripMenuItem item in scaleItems)
                {
                    item.Enabled = true;
                }

                // Enable all radio buttons
                rdbNearest.Enabled = true;
                rdbBilinear.Enabled = true;
                rdbBicubic.Enabled = true;
            }

            UpdateView();
        }
    }
}